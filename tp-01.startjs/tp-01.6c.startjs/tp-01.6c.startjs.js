/* 
***********************
*#tp-01.6c.startjs:*
***********************
 1/ afficher la liste des étudiants nom et prénom dans une liste ol
 dans cette liste
 2/ afficher pour chaque étudiant une liste ul avec chaque matière et chaque note associée
 3/ Calculer et afficher pour chaque étudiant sa moyenne générale
*/
var etudiants = [
    {
        "prenom" : "Claude",
        "nom"  : "François",
        "matieres" : {
                "français" : 13,
                "anglais" : 14,
                "sport" : 9
        }
    },
        {
            "prenom" : "Zindine",
            "nom"  : "Zidane",
            "matieres" : {
                    "français" : 2,
                    "anglais" : 15,
                    "sport" : 6,
                   
            }
        }
];

