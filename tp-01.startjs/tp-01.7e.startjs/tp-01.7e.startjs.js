/**
***********************
*#tp-01.7e.startjs*
***********************
 * Objectif :
Appeler une fonction d'une manière répétitive en utilisant la fonction setInterval().

Travail à faire :
        En se basant sur le code html (voir la page index.html)
 * 
 * Créer un chronomètre composé d'un afficheur et 3 boutons : un bouton "start" pour lancer le chronomètre , 
 * un bouton "stop" pour l’arrêter et un bouton "reset" pour le remettre à zéro.
 * aperçu du resultat : http://prntscr.com/nhzbtx
 * 
 * Indications:
 * --------------------------
 * Créer une fonction "start" qui appelle d'une manière répétitive la fonction update_chrono via la fonction setInterval avec une cadence de 100 ms.
 * Créer une fonction "update_chrono()" qui permet d’incrémenter le nombre de millisecondes , secondes , minutes , et des heures.
 * Créer une fonction "stop" qui arrête le traitement de setInterval.
 * Créer une fonction "reset" qui remet les valeurs et l'affichage à 0 .
*/


